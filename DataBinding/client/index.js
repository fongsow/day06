/**
 * Created by s17465 on 17/10/2016.
 */
var RegApp = angular.module("BindingApp", []);
(function() {
    var RegCtrl = function() {
        var ctrl = this;
        ctrl.myname  = "Leong FS";
        ctrl.age = 20;
        ctrl.pageColor = "blue";
    }
    RegApp.controller("RegCtrl", [RegCtrl]);
    }
)();