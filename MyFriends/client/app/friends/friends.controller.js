(function () {
    var app = angular.module("friendsApp");

    app.controller("friendsCtrl", ['ModalService', 'serverData',
        function (ModalService, serverData) {
        var self = this;
        self.format = 'hh:mm:ss a';
        self.id = 0;
        var defaulFriendObject = {
            id: null,
           friendName: '',
            contactNo: '',
            email: '',
            gender: 'male',
            saved: 0

        };

        serverData.getData().then(function (data) {
            console.log(data); //added fs
            self.friendsArray = data;
            self.id = data.length + 1;
        });
        self.friendObject = angular.copy(defaulFriendObject);
        self.friendsArray = [];
        self.query = '';
        self.orderProp = 'age';


        self.submit = function () {
            console.log("***" + self.friendObject.id);
            if (self.friendObject.id === null) {
                self.friendObject.id = self.id++;
            }
            if (self.friendObject.gender === 'female') {
                self.friendObject.imageUrl = 'https://upload.wikimedia.org/wikipedia/commons/0/07/Avatar_girl_face.png'
            } else {
                self.friendObject.imageUrl = 'http://i.imgur.com/1o1zEDM.png';
            }

            self.friendsArray.push(self.friendObject);
            self.friendObject = angular.copy(defaulFriendObject)

        };

        self.showUserDetails = function (friendDetails) {

            console.log(friendDetails);
            ModalService.showModal({
                templateUrl: "../app/modals/show.friend-details.modal.html",
                controller: "showUserController as ctrl",
                inputs: {
                    title: friendDetails,
                }
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {

                });
            });

        }

        self.reset = function () {
            self.friendObject = angular.copy(defaulFriendObject);
        };


        self.saveToDB = function (friendId,saveStatus) {
            if(!saveStatus){
                for (var i = 0; i < self.friendsArray.length; i++) {
                    if (self.friendsArray[i].id == friendId) {
                        serverData.saveData(self.friendsArray[i]).then(function (response) {
                            self.friendsArray = [];
                            self.friendsArray = response;
                        });
                        break;
                    }
                }
            }else{
                console.log("Friend Details are already Saved");
            }

        };


        self.delete = function (friendId) {

            serverData.deleteData(friendId).then(function (response) {
                self.friendsArray = [];
                self.friendsArray = response;
            });

        }

    }]);
}());